
  <form id="fileUploadForm" method="POST" action="{{ url('/upload') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                 <input style="line-height:1"  class="form-control" multiple name="file[]" type="file" id="formFile">
                              </div>
                            <div class="d-grid mb-3">
                                <input  type="submit" value="Submit" class="btn btn-primary">
                            </div>
                            <div class="form-group">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                </div>
                            </div>
                        </form>  
