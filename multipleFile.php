<?php

Route::post('upload', function (Request $request) {

    $request->validate(
        [
            'file.*' => 'required|mimes:jpg,jpeg,png,bmp|max:20000',
        ], [
            'file.*.required' => 'Please upload an image',
            'file.*.mimes'    => 'Only jpeg,png and bmp images are allowed',
            'file.*.max'      => 'Sorry! Maximum allowed size for an image is 20MB',
        ]
    );

    $data = array();

    foreach ($request->file as $key => $value) {

        $name = time() . $key . '.' . $value->getClientOriginalExtension();
        $value->move(storage_path('app/public/attach'), $name);
        $data[] = $name;
    }

    return response()->json(['success' => 'Successfully uploaded.', 'data' => $data]);
});
